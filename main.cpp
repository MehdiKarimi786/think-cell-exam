#include <iostream>
#include <map>
#include <limits>

template<typename K, typename V>
class interval_map {
    std::map<K,V> m_map;

public:
    // constructor associates whole range of K with val by inserting (K_min, val)
    // into the map
    interval_map( V const& val) {
        m_map.insert(m_map.end(),std::make_pair(std::numeric_limits<K>::lowest(),val));
    }

    // Assign value val to interval [keyBegin, keyEnd).
    // Overwrite previous values in this interval.
    // Conforming to the C++ Standard Library conventions, the interval
    // includes keyBegin, but excludes keyEnd.
    // If !( keyBegin < keyEnd ), this designates an empty interval,
    // and assign must do nothing.
    void assign( K const& keyBegin, K const& keyEnd, V const& val ) {
// INSERT YOUR SOLUTION HERE
        if(!(keyBegin < keyEnd ))
                return;

        auto it_begin = m_map.upper_bound(keyBegin);
        --it_begin;
        auto val_remained=it_begin->second;
        if(it_begin->first < keyBegin)
        {
            if(!(it_begin->second == val))
            {
                ++it_begin;
                it_begin=m_map.insert(it_begin,std::make_pair(keyBegin,val));
            }
        }
        else
        {
            if(!(it_begin->second == val))
            {
                if(it_begin != m_map.begin())
                {
                    auto it_tmp(it_begin);
                    --it_tmp;
                    if(it_tmp->second == val)
                    {
                        m_map.erase(it_begin);
                        it_begin=it_tmp;
                    }
                    else
                    {
                        it_begin->second=val;
                    }
                }
                else
                {
                    it_begin->second=val;
                }
            }
        }


        auto it_end = m_map.upper_bound(keyEnd);
        --it_end;
        if(it_end != it_begin)
        {
            val_remained=it_end->second;
            auto it_begin_erase(it_begin);
            ++it_begin_erase;

            auto it_end_erase(it_end);
            ++it_end_erase;

            m_map.erase(it_begin_erase, it_end_erase);
        }

        if(!(it_begin->second == val_remained))
        {
            ++it_begin;
            m_map.insert(it_begin,std::make_pair(keyEnd,val_remained));
        }


    }

    // look-up of the value associated with key
    V const& operator[]( K const& key ) const {
        return ( --m_map.upper_bound(key) )->second;
    }

    void print()
    {
        for(auto it(m_map.begin()); it != m_map.end(); ++it)
        {
            std::cout << it->first << "," << it->second << std::endl;
        }
    }
};

// Many solutions we receive are incorrect. Consider using a randomized test
// to discover the cases that your implementation does not handle correctly.
// We recommend to implement a test function that tests the functionality of
// the interval_map, for example using a map of unsigned int intervals to char.

void test0()
{
    interval_map<uint16_t,char> test('A');
    test.assign(1,std::numeric_limits<uint16_t>::max(), 'B');
    test.assign(1,std::numeric_limits<uint16_t>::max(), 'C');


    test.print();

}

void test1()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(20,40, 'C');
    test.assign(40,60, 'D');


    test.print();

}

void test2()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(20,40, 'C');
    test.assign(39,60, 'D');


    test.print();

}

void test3()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(20,40, 'C');
    test.assign(15,18, 'D');


    test.print();

}

void test4()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(20,40, 'C');
    test.assign(15,28, 'D');

    test.print();
}

void test5()
{
    interval_map<int16_t,char> test('A');
    test.assign(-10,20, 'B');
    test.assign(-20,40, 'C');
    test.assign(-15,28, 'D');

    test.print();

}

void test6()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(30,40, 'C');
    test.assign(80,128, 'D');

    test.print();

}

void test7()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(15,19, 'C');
    test.assign(20,21, 'D');

    test.print();

}

void test8()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(15,19, 'C');
    test.assign(20,20, 'D');

    test.print();
}

void test9()
{
    interval_map<uint16_t,char> test('A');
    test.assign(100,200, 'B');
    test.assign(15,19, 'C');
    test.assign(20,2000, 'D');

    test.print();

}

void test10()
{
    interval_map<int32_t,char> test('A');
    test.assign(100,200, 'B');
    test.assign(15,19, 'C');
    test.assign(20,2000, 'D');

    test.print();

}

void test11()
{
    interval_map<int32_t,char> test('A');
    test.assign(-100,200, 'B');
    test.assign(-15,19, 'C');
    test.assign(-20,-2000, 'D');

    test.print();

}

void test12()
{
    interval_map<int32_t,char> test('A');
    test.assign(-100,200, 'B');
    test.assign(-15,19, 'C');
    test.assign(-20000,-2000, 'D');

    test.print();

}

void test13()
{
    interval_map<int32_t,char> test('A');
    test.assign(-100,200, 'B');
    test.assign(-15,19, 'C');
    test.assign(-20000,-2000, 'D');
    test.assign(20000,1112000, 'D');

    test.print();

}

void test14()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'A');
    test.assign(15,25, 'A');
    test.assign(100,200, 'A');
    test.assign(120,2670, 'A');
    test.assign(10,20, 'A');

    test.print();

}

void test15()
{
    interval_map<uint16_t,char> test('A');
    test.assign(10,20, 'B');
    test.assign(10,25, 'A');
    test.print();

}

#define TEST_COUNT 16

void test(int i)
{
    std::cout << "================================================" << std::endl;
    std::cout << "Test: " << i << std::endl;
    switch(i)
    {
    case 0: test0();return;
    case 1: test1();return;
    case 2: test2();return;
    case 3: test3();return;
    case 4: test4();return;
    case 5: test5();return;
    case 6: test6();return;
    case 7: test7();return;
    case 8: test8();return;
    case 9: test9();return;
    case 10: test10();return;
    case 11: test11();return;
    case 12: test12();return;
    case 13: test13();return;
    case 14: test14();return;
    case 15: test15();return;
    }
    std::cout << "================================================" << std::endl;
}

int main()
{
    for(int i=0;i<TEST_COUNT;++i)
        test(i);

    return 0;
}
